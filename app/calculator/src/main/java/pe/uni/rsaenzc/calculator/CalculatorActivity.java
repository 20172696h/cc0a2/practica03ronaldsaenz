package pe.uni.rsaenzc.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class CalculatorActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    Animation animationImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        imageViewSplash=findViewById(R.id.imagen_view_splash);
        animationImage = AnimationUtils.loadAnimation(this,R.anim.image_animation);

        imageViewSplash.setAnimation(animationImage);

        new CountDownTimer(5000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(CalculatorActivity.this, Layout2.class);
                startActivity(intent);
                finish();
            }
        }.start();

    }
}